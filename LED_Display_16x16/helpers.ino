void DrawSquare(int xPos, int yPos, int s, CRGB color) {
  for (int x = xPos; x < xPos + s; x++) {
    SetPixel(x, yPos, color);
    SetPixel(x, yPos + s, color);
  }
  for (int y = yPos; y < yPos + s; y++) {
    SetPixel(xPos, y, color);
    SetPixel(xPos + s, y, color);
  }
  SetPixel(xPos + s, yPos + s, color);

}

int SetPixel(int idx, CRGB color){
  int x = idx % 16;
  int y = idx / 16;

  SetPixel(x, y, color);  
}

void SetPixel(int x, int y, CRGB color) {
  color.r = color.r/2;
  int pos = y * 16;
  if (y % 2 == 0) {
    pos += 15 - x;
  } else {
    pos += x;
  }
  leds[pos] = color;
}

CRGB GetPixel(int x, int y) {
  int pos = y * 16;
  if (y % 2 == 0) {
    pos += 15 - x;
  } else {
    pos += x;
  }
  return leds[pos];
}


CRGB Random() {
  return CRGB(random(255), random(255), random(255));
}

void BlackAll() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Black;
  }
}

void SetAll(CRGB color) {
  //color.r = color.r/2;
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = color;
  }
}

void DrawImage(){
  for (int i = 0; i < 256; i++){
    SetPixel(i, CRGB(image[i][0], image[i][1], image[i][2]));
  }
  FastLED.show();
}


void DrawImage(int img[][3]){
  for (int i = 0; i < 256; i++){
    SetPixel(i, CRGB(img[i][0], img[i][1], img[i][2]));
  }
  FastLED.show();
}
