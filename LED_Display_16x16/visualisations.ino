void Rain() {
  //FastLED.setBrightness(255);
  int dropCount = 16;
  float dropPosX[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  float dropPosY[] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
  float dropVel[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

  for (int i = 0; i < 500; i++) {
    BlackAll();

    for (int d = 0; d < dropCount; d++) {
      if (dropPosY[d] == 15) {
        dropPosY[d] = -1;
      }


      if (dropPosY[d] == -1) {
        if (random(100) < 5) {
          dropPosY[d] = 0;
          dropVel[d] = 0.3 + (0.1 * random(10));
        }
      } else {
        dropPosY[d] += dropVel[d];
      }

      if (dropPosY[d] > 15) {
        dropPosY[d] = 15;
      } 
    }

    for (int d = 0; d < dropCount; d++) {
      if (dropPosY[d] >= 0) {
        SetPixel(d, round(dropPosY[d]), CRGB::Blue);
      }
    }
    FastLED.show();
    delay(20);
  }

}


void RandomColours() {
  //FastLED.setBrightness(255);
  for (int i = 0; i < 100; i++) {
    for (int l = 0; l < NUM_LEDS; l++) {
      leds[l] = Random();
    }
    FastLED.show();
    delay(50);
  }

}

void Bounce() {
  //FastLED.setBrightness(255);
  int xPos = 4;
  int yPos = 0;
  int s = random(3) + 1;
  int xVel = random(2) + 1;
  int yVel = random(2) + 1;

  CRGB color = Random();

  for (int i = 0; i < 200; i++) {
    xPos += xVel;
    yPos += yVel;

    if (xPos + s > 15 || xPos < 0) {
      xVel = -xVel;
      xPos += xVel;
    }

    if (yPos + s > 15 || yPos < 0) {
      yVel = -yVel;
      yPos += yVel;
    }
    BlackAll();
    DrawSquare(xPos, yPos, s, color);
    FastLED.show();
    delay(25);
  }

}

void ExpandingSquares() {
  //FastLED.setBrightness(255);
  CRGB color = Random();

  for (int s = 0; s < 16; s++) {
    DrawSquare(0, 0, s, color);
    FastLED.show();
    BlackAll();
    delay(30);
  }

  for (int s = 0; s < 16; s++) {
    DrawSquare(s, s, 15 - s, color);
    FastLED.show();
    BlackAll();
    delay(30);
  }

  for (int s = 15; s >= 0; s--) {
    DrawSquare(0, s, 15 - s, color);
    FastLED.show();
    BlackAll();
    delay(30);
  }
  for (int s = 0; s < 16; s++) {
    DrawSquare(s, 0, 15 - s, color);
    FastLED.show();
    BlackAll();
    delay(30);
  }

  for (int s = 0; s < 16; s++) {
    DrawSquare(0, 0, s, color);
    FastLED.show();
    BlackAll();
    delay(10);
  }

  for (int s = 15 ; s >= 0; s--) {
    DrawSquare(0, 0, s, color);
    FastLED.show();
    BlackAll();
    delay(10);
  }

  for (int s = 15; s >= 0; s--) {
    DrawSquare(s, s, 15 - s, color);
    FastLED.show();
    BlackAll();
    delay(10);
  }

  for (int s = 0; s < 16; s++) {
    DrawSquare(s, s, 15 - s, color);
    FastLED.show();
    BlackAll();
    delay(10);
  }
}
