#include <FastLED.h>
#include "images.h"

// How many leds in your strip?
#define NUM_LEDS 256

#define DATA_PIN 4

// Define the array of leds
CRGB leds[NUM_LEDS];


void setup() {
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);

  FastLED.setBrightness(15);
  Serial.begin(115200);
  Serial.println("START");


  SetAll(CRGB(0, 0, 0));
  FastLED.show();
  delay(200);

}

void loop() {
  DisplayLoop();
}

void DisplayLoop() {
  for (int i = 0; i < 4; i++) {
    DrawImage(mario0);
    delay(200);
    DrawImage(mario1);
    delay(200);
    DrawImage(mario2);
    delay(200);
    DrawImage(mario3);
    delay(200);
  }
  DrawImage(ironman);
  delay(1000);
  DrawImage(thor);
  delay(1000);
  DrawImage(captamerica);
  delay(1000);
  DrawImage(hulk);
  delay(1000);
  DrawImage(pickaxe);
  delay(1000);

  Rain();
  Bounce();
  ExpandingSquares();
  ExpandingSquares();
}
