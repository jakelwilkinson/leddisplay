Need to install Arduino ESP32 library first


Add this in Arduino IDE to File->Preferences->Additional Boards Manager URLs
https://dl.espressif.com/dl/package_esp32_index.json

Then Tools->Boards->Board Manager
and install "ESP32 by Espressif Systems"

Now you can choose "ESP32 Dev Kit Module" from File->Boards

Choose port and upload



New images can be added to file "images.h"
16x16 pixel images can be converted to image data at http://188.166.242.48/led/

Copy and paste the data into images.h and change the name of the variable.

It can them be added to a DrawImage(IMAGENAME); in the main file.